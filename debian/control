Source: likwid
Maintainer: Christoph Martin <martin@uni-mainz.de>
Section: misc
Priority: optional
Standards-Version: 3.9.8
Build-Depends: debhelper (>= 9), dpkg-dev (>= 1.16.1~), gfortran, liblua5.2-dev
Homepage: https://github.com/rrze-likwid/likwid
Vcs-Git: https://salsa.debian.org/debian/likwid.git
Vcs-Browser: https://salsa.debian.org/debian/likwid

Package: likwid
Architecture: amd64 i386 x32
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends}, ${misc:Depends}, ${perl:Depends}, lua5.2
Suggests: feedgnuplot
Description: toolsuite for performance oriented programmers
 Likwid is a simple to install and use toolsuite of command line applications
 for performance oriented programmers. It works for Intel and AMD processors
 on the Linux operating system.
 .
 It consists of:
 .
 likwid-topology       - print thread and cache topology
 likwid-features       - view and toggle feature reagister on Intel processors
 likwid-perfctr        - configure and read out hardware performance counters
                         on Intel and AMD processors
 likwid-powermeter     - read out RAPL Energy information and get info about
                         Turbo Mode steps
 likwid-setFrequencies - read out RAPL Energy information and get info about
                         Turbo Mode steps
 likwid-memsweeper     - cleans up filled NUMA memory domains and evicts dirty
                         cacheline from cache hierarchy
 likwid-pin            - pin your threaded application (pthread, Intel and gcc
                         OpenMP to dedicated processors
 likwid-bench          - Micro benchmarking platform
 likwid-gencfg         - Dumps topology information to a file
 likwid-mpirun         - Wrapper to start MPI and Hybrid MPI/OpenMP
                         applications (Supports Intel MPI and OpenMPI)
 likwid-scope          - Frontend to the timeline mode of likwid-perfctr, plots
                         live graphs of performance metrics
