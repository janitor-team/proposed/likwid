--- /dev/null
+++ b/doc/feedGnuplot.1
@@ -0,0 +1,190 @@
+.TH feedGnuplot 1 <DATE> likwid\-<VERSION>
+.SH NAME
+feedGnuplot \- General purpose pipe-oriented plotting tool
+.SH SYNOPSIS
+.B likwid-setFreq 
+.IR <coreId>
+.IR <frequency>
+.IR [<governor>]
+
+.SH DESCRIPTION
+.B feedGnuplot
+is a pipe-oriented plotting frontend for GNUplot that can read internediate results and create a sort of live plot of the data.
+.B feedGnuplot
+is used by
+.B likwid-perfscope(1)
+to print performance counter data printed out by the timeline daemon mode of
+.B likwid-perfctr(1).
+The Perl script
+.B feedGnuplot
+is not written by the LIKWID Authors, it was written by Dima Kogan and published under GPL. The original web page is https://github.com/dkogan/feedgnuplot
+.SH OPTIONS
+.TP
+.B \-h
+prints a help message to standard output, then exits.#
+.TP
+.B \-\-[no]domain
+If enabled, the first element of each line is the domain variable.  If not, the point index is used.
+.TP
+.B \-\-[no]dataid
+If enabled, each data point is preceded by the ID of the data set that point corresponds to. This ID is
+interpreted as a string, NOT as just a number. If not enabled, the order of the point is used.
+.TP
+.B \-\-[no]3d
+Do [not] plot in 3D. This only makes sense with 
+.B --domain.
+Each domain here is an (x,y) tuple.
+.TP
+.B \-\-colormap
+Show a colormapped xy plot. Requires extra data for the color. zmin/zmax can be used to set the extents of the colors.
+Automatically increments extraValuesPerPoint.
+.TP
+.B \-\-[no]stream
+Do [not] display the data a point at a time, as it comes in.
+.TP
+.B \-\-[no]lines
+Do [not] draw lines to connect consecutive points.
+.TP
+.B \-\-[no]points
+Do [not] draw points.
+.TP
+.B \-\-circles
+Plot with circles. This requires a radius be specified for each point. Automatically increments extraValuesPerPoint.
+.TP
+.B \-\-xlabel " xxx
+Set x-axis label.
+.TP
+.B \-\-ylabel " xxx
+Set y-axis label.
+.TP
+.B \-\-y2label " xxx
+Set y2-axis label. Does not apply to 3d plots.
+.TP
+.B \-\-zlabel " xxx
+Set z-axis label. Only applies to 3d plots.
+.TP
+.B \-\-title " xxx
+Set the title of the plot.
+.TP
+.B \-\-legend " curveID=legend
+Set the label for a curve plot. Use this option multiple times for multiple curves. With 
+.B --dataid
+, curveID is the ID. Otherwise, it's the index of the curve, starting at 0.
+.TP
+.B \-\-autolegend
+Use the curve IDs for the legend. Titles given with
+.B --legend
+override these.
+.TP
+.B \-\-xlen " xxx
+When using 
+.B --stream
+, sets the size of the x-window to plot. Omit this or set it to 0 to plot ALL the data. Does not make sense with 3d plots. Implies
+.B --monotonic
+.TP
+.B \-\-xmin " xxx
+Set the minimal point in range for the x-axis. These are ignored in a streaming plot.
+.TP
+.B \-\-xmax " xxx
+Set the maximal point in range for the x-axis. These are ignored in a streaming plot.
+.TP
+.B \-\-ymin " xxx
+Set the minimal point in range for the y-axis.
+.TP
+.B \-\-ymax " xxx
+Set the maximal point in range for the y-axis.
+.TP
+.B \-\-y2min " xxx
+Set the minimal point in range for the y2-axis. Does not apply to 3d plots.
+.TP
+.B \-\-y2max " xxx
+Set the maximal point in range for the y2-axis. Does not apply to 3d plots.
+.TP
+.B \-\-zmin " xxx
+Set the minimal point in range for the z-axis. Only applies to 3d plots or colormaps.
+.TP
+.B \-\-zmax " xxx
+Set the maximal point in range for the z-axis. Only applies to 3d plots or colormaps.
+.TP
+.B \-\-y2 " xxx
+Plot the data specified by this curve ID on the y2 axis. Without
+.B --dataid
+, the ID is just an ordered 0-based index. Does not apply to 3d plots.
+.TP
+.B \-\-curvestyle " curveID=style
+Additional styles per curve. With
+.B --dataid
+, curveID is the ID. Otherwise, it's the index of the curve, starting at 0. Use this option multiple times for multiple curves.
+.TP
+.B \-\-curvestyleall " xxx
+Additional styles for ALL curves.
+.TP
+.B \-\-extracmds " xxx
+Additional commands. These could contain extra global styles for instance.
+.TP
+.B \-\-size " xxx
+Gnuplot size option.
+.TP
+.B \-\-square
+Plot data with aspect ratio 1. For 3D plots, this controls the aspect ratio for all 3 axes.
+.TP
+.B \-\-square_xy
+For 3D plots, set square aspect ratio for ONLY the x,y axes.
+.TP
+.B \-\-hardcopy " xxx
+If not streaming, output to a file specified here. Format inferred from filename.
+.TP
+.B \-\-maxcurves " xxx
+The maximum allowed number of curves. This is 100 by default, but can be reset with this option. This exists purely to prevent perl from allocating all of the system's memory when reading bogus data.
+.TP
+.B \-\-monotonic
+If
+.B --domain
+is given, checks to make sure that the x-coordinate in the input data is monotonically increasing.If a given x-variable is in the past, all data currently cached for this curve is purged. Without 
+.B --monotonic
+, all data is kept. Does not make sense with 3d plots. No 
+.B --monotonic
+by default.
+.TP
+.B \-\-extraValuesPerPoint " xxx
+How many extra values are given for each data point. Normally this is 0, and does not need to be specified, but sometimes we want extra data, like for colors or point sizes or error bars, etc.
+.B feedGnuplot
+options that require this (colormap, circles) automatically set it. This option is ONLY needed if unknown styles are used, with 
+.B --curvestyleall
+for instance.
+.TP
+.B \-\-dump
+Instead of printing to gnuplot, print to STDOUT. For debugging.
+
+.SH EXAMPLE
+.IP 1. 4
+Simple real-time plotting example: plot how much data is received on the wlan0 network interface in bytes/second
+.TP
+.B while true; do sleep 1; cat /proc/net/dev; done | gawk '/wlan0/ {if(b) {print $2-b; fflush()} b=$2}' | \\
+.B feedgnuplot --lines --stream --xlen 10 --ylabel 'Bytes/sec' --xlabel seconds
+.PP
+Reads the stats of the network interface 'wlan0' every second, reformats it with
+.B gawk
+and pipes the formated output into
+.B feedGnuplot
+qto create a line plot (
+.B --lines
+) of the streaming input (
+.B --stream
+). Always show the last 10 seconds (
+.B --xlen
+) and use the labels 'seconds' for the x-axis and 'Bytes/sec' for the y-axis.
+.IP 2. 4
+Simple real-time plotting example: plot the 'idle' CPU consumption against time
+.TP
+.B sar 1 -1 | awk '$1 ~ /..:..:../ && $8 ~/^[0-9\.]*$/ {print $1,$8; fflush()}' | \\
+.B feedgnuplot --stream --domain --lines --timefmt '%H:%M:%S' --set 'format x "%H:%M:%S"'
+.PP
+Reads the CPU IDLE consumption and sets the current time as x-axis key.
+
+.SH AUTHOR
+Written by Dima Kogan <dima@secretsauce.net>.
+.SH BUGS
+Report Bugs on <https://github.com/dkogan/feedgnuplot/issues>.
+.SH "SEE ALSO"
+gnuplot(1), awk(1), sar(1),  likwid-perfscope(1), likwid-perfctr(1)
--- /dev/null
+++ b/doc/likwid-accessD.1
@@ -0,0 +1,22 @@
+.TH LIKWID-ACCESSD 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-accessD \- This tool forwards the access operations from LIKWID PerfMon tools
+to the MSR device files
+.SH DESCRIPTION
+.B likwid-accessD
+is a command line application that opens a UNIX file socket and waits for access
+operations from LIKWID tools that require access to the MSR and PCI device
+files. The MSR and PCI device files are only accessible for users with root
+privileges, therefore
+.B likwid-accessD
+requires the suid-bit set.
+Depending on the current system architecture,
+.B likwid-accessD
+permits only access to registers defined for the architecture.
+
+.SH AUTHOR
+Written by Thomas Roehl <thomas.roehl@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-perfctr(1), likwid-powermeter(1), likwid-features(1), likwid-pin(1), likwid-topology(1),
--- /dev/null
+++ b/doc/likwid-genCfg.1
@@ -0,0 +1,30 @@
+.TH LIKWID-GENCFG 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-genCfg \- Get system topology and write them to file for faster LIKWID startup
+.SH SYNOPSIS
+.B likwid-genCfg
+.RB [\-hv]
+.RB [ \-o
+.IR <filename>]
+.SH DESCRIPTION
+.B likwid-genCfg
+is a command line application that stores the system's CPU and NUMA topology to
+file. LIKWID applications use this file to read in the topology fast instead of
+re-gathering all values. The default output path is /etc/likwid.cfg.
+.SH OPTIONS
+.TP
+.B \-h
+prints a help message to standard output, then exits.
+.TP
+.B \-v
+prints a version message to standard output, then exits.
+.TP
+.B \-\^o " <filename>
+sets output file path (optional)
+
+.SH AUTHOR
+Written by Thomas Roehl <thomas.roehl@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-topology(1), likwid-perfctr(1), likwid-features(1), likwid-pin(1), likwid-powermeter(1)
--- /dev/null
+++ b/doc/likwid-memsweeper.1
@@ -0,0 +1,28 @@
+.TH LIKWID-MEMSWEEPER 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-memsweeper \- A tool to clean up NUMA memory domains and last level caches.
+.SH SYNOPSIS
+.B likwid-memsweeper
+.RB [\-hv]
+.RB [ \-c
+.IR <NUMA_ID> ]
+.SH DESCRIPTION
+.B likwid-memsweeper
+is a command line application to shrink the file buffer cache by filling the NUMA domain with random pages. Moreover the tool invalidates all cachelines in the LLC.
+.SH OPTIONS
+.TP
+.B \-h
+prints a help message to standard output, then exits.
+.TP
+.B \-v
+prints a version message to standard output, then exits.
+.TP
+.B \-\^c " <NUMA_ID>
+set the NUMA domain for sweeping.
+
+.SH AUTHOR
+Written by Thomas Roehl <thomas.roehl@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-perfctr(1), likwid-features(1), likwid-pin(1), likwid-powermeter(1), likwid-topology(1),
--- /dev/null
+++ b/doc/likwid-mpirun.1
@@ -0,0 +1,81 @@
+.TH LIKWID-MPIRUN 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-mpirun \- A tool to start and monitor MPI applications with LIKWID
+.SH SYNOPSIS
+.B likwid-memsweeper
+.RB [\-hd]
+.RB [ \-hostfile
+.IR filename ]
+.RB [ \-nperdomain
+.IR number_of_processes_in_domain ]
+.RB [ \-pin
+.IR expression ]
+.RB [ \-omp
+.IR expression ]
+.RB [ \-mpi
+.IR expression ]
+.RB [\-\-]
+.SH DESCRIPTION
+.B likwid-mpirun
+is a command line application that wraps the vendor-specific mpirun tool and adds calls to
+.B likwid-perfctr(1)
+to the execution string. The user-given application is ran, measured and the results returned to the staring node.
+.SH OPTIONS
+.TP
+.B \-h
+prints a help message to standard output, then exits.
+.TP
+.B \-d
+prints debug messages to standard output.
+.TP
+.B \-\^hostfile " filename
+specifies the nodes to schedule the MPI processes on
+.TP
+.B \-\^nperdomain " number_of_processes_in_domain
+specifies the processes per affinity domain (see
+.B likwid-pin
+for info about affinity domains)
+.TP
+.B \-\^pin " expression
+specifies the pinning for hybrid execution (see
+.B likwid-pin
+for info about affinity domains)
+.TP
+.B \-\^omp " expression
+enables hybrid setup. Can only be used in combination with
+.B -pin.
+The only possible value is: intel
+.TP
+.B \-\^mpi " expression
+specifies the MPI implementation that should be used by the wrapper. Possible values are intelmpi, openmpi and mvapich2
+.TP
+.B \-\-
+stops parsing arguments for likwid-mpirun, in order to set options for underlying MPI implementation after \-\-.
+
+.SH EXAMPLE
+.IP 1. 4
+For standard application:
+.TP
+.B likwid-mpirun -np 32  ./myApp
+.PP
+Will run 32 MPI processes, each host is filled with as much processes as written in ppn
+.IP 2. 4
+With pinning:
+.TP
+.B likwid-mpirun -np 32 -nperdomain S:2  ./myApp
+.PP
+Will start 32 MPI processes with 2 processes per socket.
+.IP 3. 4
+For hybrid runs:
+.TP
+.B likwid-mpirun -np 32 -pin M0:0-3_M1:0-3  ./myApp
+.PP
+Will start 32 MPI processes with 2 processes per node. Threads of the first process are pinned to the cores 0-3 in NUMA domain 0 (M0). The OpenMP threads of the second process are pinned to the first four cores in NUMA domain 1 (M1)
+
+
+.SH AUTHOR
+Written by Thomas Roehl <thomas.roehl@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-pin(1), likwid-perfctr(1), likwid-features(1), likwid-powermeter(1), likwid-topology(1),
--- /dev/null
+++ b/doc/likwid-perfscope.1
@@ -0,0 +1,55 @@
+.TH LIKWID-PERFSCOPE 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-perfscope \- Frontend for the timeline mode of
+.B likwid-perfctr(1)
+that on-the-fly generates pictures from the measurements
+.SH SYNOPSIS
+.B likwid-perfscope 
+.RB [\-h]
+.RB [ \-cores
+.IR <cpu_list> ]
+.RB [ \-freq
+.IR <frequency> ]
+.RB [ \-group
+.IR <eventset> ]
+.SH DESCRIPTION
+.B likwid-perfscope
+is a command line application written in Perl that uses the timeline daemon mode of
+.B likwid-perfctr(1)
+to create on-the-fly pictures with the current measurements. It uses the
+.B feedGnuplot(1)
+script to send the current data to gnuplot.
+.SH OPTIONS
+.TP
+.B \-h
+prints a help message to standard output, then exits.
+.TP
+.B \-\^cores " <cpu_list>
+measures the given group on given CPUs in <cpu_list>
+.TP
+.B \-\^freq " <frequency>
+reads the current performance values every <frequency>. Available suffixes are 's' and 'ms', e.g. 500ms. Default value is 1s
+.TP
+.B \-\^group " <eventset>
+defines the events and counters that should be read. Possible values can be gathered from
+.B likwid-perfctr(1).
+Default is group 'FLOPS_DP'
+
+.SH EXAMPLE
+.IP 1. 4
+Monitor double precision floating-point operations:
+.TP
+.B likwid-perfscope -group FLOPS_DP -cores 0-3 -freq 500ms
+.PP
+Executes
+.B likwid-perfctr
+on the first four cores. The values are read every 500ms are forwarded to gnuplot using the
+.B feedGnuplot
+script.
+
+.SH AUTHOR
+Written by Jan Treibig <jan.treibig@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-perfctr(1), feedGnuplot(1), likwid-pin(1), likwid-powermeter(1), likwid-setFrequencies(1)
--- /dev/null
+++ b/doc/likwid-setFreq.1
@@ -0,0 +1,24 @@
+.TH LIKWID-SETFREQ 1 <DATE> likwid\-<VERSION>
+.SH NAME
+likwid-setFreq \- Mediator for
+.B likwid-setFrequencies(1)
+that performs the actual setting of CPU cores' frequency and governor.
+.SH SYNOPSIS
+.B likwid-setFreq 
+.IR <coreId>
+.IR <frequency>
+.IR [<governor>]
+
+.SH DESCRIPTION
+.B likwid-setFreq
+is a command line application that mediates the request from
+.B likwid-setFrequencies(1)
+because setting a CPU core's frequency and/or governor requires root privileges. This executable must be suid-root.
+
+
+.SH AUTHOR
+Written by Jan Treibig <jan.treibig@gmail.com>.
+.SH BUGS
+Report Bugs on <http://code.google.com/p/likwid/issues/list>.
+.SH "SEE ALSO"
+likwid-setFrequencies(1), likwid-perfctr(1), feedGnuplot(1), likwid-pin(1), likwid-powermeter(1)
